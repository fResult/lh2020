import React, { useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import HomeLiff from './pages/HomeLiff'
import './App.css'
import TodoApp from './pages/TodoApp'
import VConsole from 'vconsole/dist/vconsole.min'

const routes = [
  { path: '/', key: 'homeLiff', component: HomeLiff },
  { path: '/todo-app', key: 'todoApp', Component: TodoApp }
]
function App({ liff }) {
  useEffect(() => {
    if (liff.isInClient()) {
      new VConsole()
    }
  })

  return (
    <div className="App">
      <Router>
        <Link to="/">LIFF</Link>
        <Link to="/todo-app">Todo</Link>
        <Switch>
          {routes.map(({ path, key, Component }) => (
            <Route {...{ path, key }}>
              <Component />
            </Route>
          ))}
        </Switch>
      </Router>
      <HomeLiff />
    </div>
  )
}

export default App
