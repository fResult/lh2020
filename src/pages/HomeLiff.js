import React, { useCallback, useContext, useEffect, useState } from 'react'
import { Button } from 'antd'
import { LiffContext } from '../index'

const HomeLiff = (props) => {
  const [name, setName] = useState('')
  const [userLineID, setUserLineID] = useState('')
  const [pictureUrl, setPictureUrl] = useState('')
  const [statusMessage, setStatusMessage] = useState('')
  const [email, setEmail] = useState('')

  const [os, setOS] = useState('')
  const [language, setLanguage] = useState('')
  const [version, setVersion] = useState('')
  const [accessToken, setAccessToken] = useState('')
  const [isInClient, setIsInClient] = useState(false)

  const [resultScanCode, setResultScanCode] = useState('')

  const liff = useContext(LiffContext)

  const getEnvironment = useCallback(async () => {
    await liff.init({ liffId: '1655037033-Dko2yjx2' })
    setOS(liff.getOS())
    setLanguage(liff.getLanguage())
    setVersion(liff.getVersion())
    setAccessToken(liff.getAccessToken())
    setIsInClient(liff.isInClient())
  }, [liff])

  const getProfile = useCallback(async () => {
    await liff.init({ liffId: '1655037033-Dko2yjx2' })
    liff.getProfile().then((profile) => {
      setName(profile.displayName)
      setUserLineID(profile.userId)
      setPictureUrl(profile.pictureUrl)
      setStatusMessage(profile.statusMessage)
      setEmail(liff.getDecodedIDToken().email)
    })
  }, [liff])

  const scanQrCode = async () => {
    const result = await liff.scanCode()
    setResultScanCode(result.value)
  }

  const start = useCallback(async () => {
    await liff.ready
    if (liff.isLoggedIn()) {
      await getProfile()
      getEnvironment()
    } else {
      liff.login()
    }
  }, [getEnvironment, getProfile, liff])

  useEffect(() => {
    (async () => await start())()
  }, [start])

  function sendMessage() {
    liff
      .sendMessages([
        {
          type: 'text',
          text: `Hi LIFF, 
            Name: ${name}
            Line ID: ${userLineID}
            Picture: ${pictureUrl}
            Status Message: ${statusMessage}
          `
        }
      ])
      .then(() => {
        liff.closeWindow()
      })
  }

  function closeLIFF() {
    liff.logout()
    liff.closeWindow()
  }

  return (
    <>
      <header className="App-header">
        <div className="support">
          <img
            width="25%"
            src="https://lh3.googleusercontent.com/illfpW97yh9TtvtmtN-BiNcpomys5gzAj4nw8Je6Ydby814PRquAPcvsP2tAV43Iqe8logzjUnjp7tN5Dvk"
            alt="FIREBASE ICON"
          />
        </div>
        <div className="environment-section">
          {<div>Your OS: {os}</div>}
          {<div>Your Language: {language}</div>}
          {<div>LIFF Version: {version}</div>}
          {<div>Your Access Token: {accessToken}</div>}
          {<div>In Line App ?: {isInClient ? 'Yes' : 'No'}</div>}
        </div>
        <div className="user-section">
          <div className="support">
            {pictureUrl && (
              <img width="25%" src={pictureUrl} alt={pictureUrl} />
            )}
          </div>
          {name && <p>Your name: {name}</p>}
          {userLineID && <p>Your LineID: {userLineID}</p>}
          {statusMessage && <p>Your statusMessage: {statusMessage}</p>}
          {email && <p>Email: {email}</p>}
        </div>
        <div className="code-section">
          {resultScanCode && <p>Code: {resultScanCode}</p>}
        </div>

        <div
          className="action-buttons"
          style={{ display: 'flex', flexFlow: 'row wrap' }}
        >
          <Button
            type="primary"
            onClick={getProfile}
            style={{ marginRight: '20px' }}
            color="primary"
          >
            Getdata INFO
          </Button>
          <Button
            type="dashed"
            onClick={sendMessage}
            style={{ marginRight: '20px' }}
          >
            Send Message
          </Button>
          <Button
            type="default"
            onClick={closeLIFF}
            color="secondary"
            style={{ background: 'lightpink' }}
          >
            Close LIFF
          </Button>
          <Button
            onClick={() =>
              liff.openWindow({
                url: 'https://fResult.dev',
                external: false
              })
            }
          >
            Open fResult.dev
          </Button>
          <Button onClick={scanQrCode}>Scan QR Code</Button>
        </div>
      </header>
    </>
  )
}

HomeLiff.propTypes = {}

export default HomeLiff
